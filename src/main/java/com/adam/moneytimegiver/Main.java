package com.adam.moneytimegiver;

import java.util.ArrayList;
import java.util.List;

import net.milkbowl.vault.economy.Economy;

import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;

import com.codestripdev.configs.LangConfigAccessor;
import com.codestripdev.configs.YamlConfigAccessor;
import com.codestripdev.helpers.CodeStripPlugin;
import com.codestripdev.helpers.ConsoleLogger;
import com.codestripdev.helpers.VaultEco;
import com.github.SagaciousZed.ConfigAccessor;

public class Main extends JavaPlugin {
	public CodeStripPlugin pluginEssentials;
	public Economy economy;
	public ConsoleLogger log;
	public ConfigAccessor lang;
	public ConfigAccessor mainConfig;

	private BukkitTask heartBeat;
	protected List<String> playersInGrace;

	public void onEnable() {

		// Set up basics
		pluginEssentials = CodeStripPlugin.instance;
		log = new ConsoleLogger(this.getDescription().getName());
		
		//Debug Settings
		//log.remotesEnabled = true;
		
		mainConfig = pluginEssentials.getConfig(new YamlConfigAccessor(this,
				this.log, "config"));
		economy = new VaultEco(this).getEco();
		lang = pluginEssentials.getConfig(new LangConfigAccessor(this,
				this.log, "messages", "en"));

		// Check if economy is loaded
		if (economy != null) {
			log.info(lang.getConfig().getString("eco.load.done"));
		} else {
			log.severe(lang.getConfig().getString("eco.load.fail"));
			getServer().getPluginManager().disablePlugin(this);
		}
		
		// Set up the grace list
		playersInGrace = new ArrayList<String>();

		// If all is well then start the task and events
		heartBeat = getServer().getScheduler().runTaskTimer(this,
				new MoneyTimer(this), 0,
				mainConfig.getConfig().getInt("mins") * 20 * 60);
		pluginEssentials.registerEvents(this, new PlayerEvents(this));
	}

	public void onDisable() {

		getServer().getScheduler().cancelTask(heartBeat.getTaskId());

	}
}
