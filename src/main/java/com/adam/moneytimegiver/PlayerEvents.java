package com.adam.moneytimegiver;

import java.util.HashMap;

import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitTask;

import com.codestripdev.helpers.EventBase;

public class PlayerEvents extends EventBase implements Listener {

	Main plugin;
	HashMap<String, BukkitTask> GraceTimers;

	public PlayerEvents(Main plugin) {
		this.plugin = plugin;
		this.GraceTimers = new HashMap<String, BukkitTask>();
	}

	public void onPlayerJoin(PlayerJoinEvent e) {
		String pName = e.getPlayer().getName();
		if (GraceTimers.containsKey(pName)) {
			plugin.getServer().getScheduler()
					.cancelTask(GraceTimers.get(pName).getTaskId());
			GraceTimers.remove(pName);
			plugin.playersInGrace.remove(pName);
		}
	}

	public void onPlayerQuit(PlayerQuitEvent e) {
		String pName = e.getPlayer().getName();
		if (GraceTimers.containsKey(pName))
			plugin.getServer().getScheduler()
					.cancelTask(GraceTimers.get(pName).getTaskId());
		GraceTimers.put(
				pName,
				plugin.getServer()
						.getScheduler()
						.runTaskLater(
								plugin,
								new PlayerQuitGraceTimer(pName),
								plugin.mainConfig.getConfig().getInt(
										"graceperiod") * 20 * 60));
	}

	public class PlayerQuitGraceTimer implements Runnable {
		String pName;

		public PlayerQuitGraceTimer(String pName) {
			this.pName = pName;
			plugin.playersInGrace.add(pName);
		}

		@Override
		public void run() {
			plugin.playersInGrace.remove(pName);
			GraceTimers.remove(pName);
		}

	}
}
