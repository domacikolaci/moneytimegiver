package com.adam.moneytimegiver;

import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

public class MoneyTimer implements Runnable {

	Main plugin;

	public MoneyTimer(Main plugin) {
		this.plugin = plugin;
	}

	@Override
	public void run() {
		for (OfflinePlayer p : plugin.getServer().getOfflinePlayers()) {
			if (p.isOnline()) {
				plugin.economy.depositPlayer(p.getName(), plugin.mainConfig
						.getConfig().getDouble("online-amount"));
				((Player) p).sendMessage(ChatColor.BOLD
						+ "You have now recevied "
						+ plugin.mainConfig.getConfig().getDouble(
								"online-amount") + "Your new balance is, "
						+ plugin.economy.getBalance(p.getName()));
			} else if (!plugin.playersInGrace.contains(p.getName()))
				plugin.economy.withdrawPlayer(p.getName(), plugin.mainConfig
						.getConfig().getDouble("offline-amount"));
		}

	}

}
