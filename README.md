#[MoneyTimeGiver][BukkitDev]

Ever Wanted To Have A Reason For Users To Keep Playing On Bukkit Severs? Well here it is!

With this plugin every certain amount of minutes the server will go through ever player who has ever played on your server and find out if they are online or not.
And depending on wether they are online the plugin will take give money to the player!

##Config
 - `mins`: The amount of minutes between each check.
 - `online-amount`: The amount of money that is given to each player who is online.
 - `offline-amount`: The amount of money that is taken from each player who is online.
 - `graceperiod`: The amount of time that the user is allowed to bypass the offline checks. This is included to allow for internet mishaps.

##Permissions
None Just Yet!

##Commands
None Just Yet!


[BukkitDev]: http://dev.bukkit.org/bukkit-plugins/